<?php


namespace App\Services;


use App\Helpers\SendRequestHelpers;
use phpDocumentor\Reflection\Types\Self_;

/**
 * Class PromElectronicaService
 * @package App\Services
 */

class PromElectronicaService
{

    /**
     * @param $method
     * @param array $data
     * @return array
     */
    private static function Send($method, $data = [])
    {
        $req = new SendRequestHelpers();
        return $req->send($method, $data);
    }

    //----------------- ПОЛУЧЕНИЕ ИНФОРМАЦИИ ---------------------------

    /**
     * Получение информации о конкретном товаре (цены, наличие, параметры)
     *
     * @param int $item_id
     * @return array
     */
    public static function item_data_get(int $item_id)
    {
        return self::Send('item_data_get', ["item_id" => $item_id]);
    }

    /**
     * Получение справочника товаров
     *
     * Описание: Первый запрос отправляется с $item_id = 0. Если в ответе параметр next_page = true, тогда $item_id = параметру item_id из ответа
     *
     * @param int $item_id
     * @return array
     */
    public static function items_data_get(int $item_id = 0)
    {
        $res = self::Send('items_data_get', ($item_id > 0) ? ["item_id" => $item_id] : []);
        $next_page = (count($res['result']) == 1000);
        $res['next_page'] = $next_page;
        $res['item_id'] = ($next_page) ? $res['result'][999]['item_id'] : 0;
        return $res;
    }

    // поиск товаров по наименованию
    public static function items_data_find()
    {

    }

    /*
     * ЗАПРОСЫ
     * */

    //добавление запроса
    public static function order_ins()
    {

    }

    //удаление запроса
    public static function order_del()
    {

    }

    // добавление товара в запрос
    public static function order_item_ins()
    {

    }

    // удаление товара из запроса
    public static function order_item_del()
    {

    }

    // изменение количества товара в запросе
    public static function order_item_quant()
    {

    }

    //получение списка товаров запроса
    public static function order_items_get()
    {

    }

    //получение списка запросов
    public static function orders_data_get()
    {
        return self::Send('orders_data_get');
    }

    //изменение состояния товаров в запросе
    public static function order_items_status_set()
    {

    }

    /*
     * ЗАКАЗЫ
     * */

    //добавление заказа
    public static function bill_ins($customer_id)
    {
        return self::Send('bill_ins', ["customer_id" => $customer_id]);
    }

    //удаление заказа
    public static function bill_del()
    {

    }

    // добавление товара в заказ
    public static function bill_item_ins()
    {

    }

    // удаление товара из заказа
    public static function bill_item_del()
    {

    }

    // изменение количества товара в заказе
    public static function bill_item_quant()
    {

    }

    //получение списка товаров заказа
    public static function bill_items_get()
    {

    }

    // получение информации о заказе
    public static function bill_items_status_set()
    {

    }

    // изменение состояния товаров в заказе
    public static function bill_status_set()
    {

    }

    // Получение списка отгрузок заказа
    public static function bill_shipments_get()
    {

    }

    //Получение списка отгрузок
    public static function shipments_data_get()
    {

    }

    /*
     * ОТГРУЗКИ
     * */

    //Получение списка товаров отгрузки
    public static function shipment_items_get()
    {

    }

    //Получение сведений об отгрузке
    public static function shipment_data_get()
    {

    }

    //-------------- ЮР ЛИЦА -----------------------------

    //получение списка юр.лиц дилера
    public static function customers_data_get()
    {
        return self::Send('customers_data_get');
    }

    //получение списка юр.лиц ПЭ (поставщиков)
    public static function providers_data_get()
    {
        return self::Send('providers_data_get');
    }

}
