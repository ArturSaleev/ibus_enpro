<?php


namespace App\Helpers;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Pool;


class SendRequestHelpers
{
    protected $url;
    public function __construct()
    {
        $on_dev = (!env('API_ON_DEV')) ? false : env('API_ON_DEV');
        $this->url = ($on_dev) ?
            [env("API_DEV1"), env("API_DEV2"), env("API_DEV3"), env("API_DEV4")] :
            [env("API_TEST1"), env("API_TEST2"), env("API_TEST3"), env("API_TEST4")]
        ;
    }

    public function send($method, $body = [])
    {
        //Устанавливаем переменные
        $attempt = 0;
        $message = "";

        //Объеденяем масивы
        $data = array_merge([
            "login" => env('API_LOGIN_TEST'),
            "password" => env('API_LOGIN_PASSWORD'),
            "method" => $method,
        ], $body);

        /**
         * Отправляем запрос.
         * В случае если запрос вернет ошибку то запрос будет переброшен на 1 из других серверов
         * Иначе выдаст ошибку
         */
        $response = Http::retry(4, 1000)->withHeaders([
                    "Content-Type" => 'application/json',
                    "Accept" => "application/json"
                ])->post($this->url[$attempt], $data);

        //------------------------------------------------------------
        $success = $response->ok();
        $data = $response->json();

        if(isset($data['error'])){
            $message = "#".$data['error_id']." - ".$data['error'];
            $success = false;
            $data = [];
        }

        return [
            "success" => $success,
            "status" => $response->status(),
            "message" => $message,
            "result" => $data
        ];

    }
}
